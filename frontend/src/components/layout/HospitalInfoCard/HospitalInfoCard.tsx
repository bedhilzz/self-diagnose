import React from 'react';

import { Text } from 'components/design-system/components/Typography';
import { Box } from 'components/design-system/components/Box';
import Link from 'next/link';
import { ChevronIcon, Phone } from 'components/icons';

export interface HospitalInfoProps {
  id: number,
  name: string;
  address: string;
  phoneNumber: string;
}

const HospitalInfoCard: React.FC<HospitalInfoProps> = ({
  id,
  name,
  address,
  phoneNumber
}) => (
    <Box
      display="flex"
      flexDirection="column"
      backgroundColor="accents03"
      color="foreground"
      borderRadius={5}
      py="md"
      px="md"
      my="md"
    >
      <Box
        display="flex"
        flexDirection="row"
        alignItems="center"
        onClick={() => { window.location.href = `/rs-rujukan-detail?id=${id}` }}
        style={{ cursor: 'pointer' }}
      >
        <Box
          display="flex"
          flexDirection="row"
          alignItems="center"
          pr="md"
          overflowX="hidden"
        >
          <Text
            as="p"
            variant={600}
            style={{
              whiteSpace: "nowrap",
              overflow: "hidden",
              textOverflow: "ellipsis"
            }}
          >
            {name}
          </Text>
        </Box>
        <Box
          display="flex"
          flexDirection="row"
          justifyContent="flex-end"
          flex="1 1 auto"
        >
          <ChevronIcon />
        </Box>
      </Box>
      <Text as="p" mt="md" fontWeight={300} color="accents07">{address}</Text>
      <Box
        as="footer"
        display="flex"
        flexDirection="row"
        alignItems="center"
        mt="md"
      >
        <Box
          display="flex"
          flexDirection="row"
          alignItems="center"
        >
          <Phone /><Text ml="sm">{phoneNumber}</Text>
        </Box>
        <Box
          display="flex"
          flexDirection="row"
          justifyContent="flex-end"
          flex="1 1 auto"
          height={24}
        >
          <Text>
            <Link href={`/rs-rujukan-detail?id=${id}`}>Lihat Peta</Link>
          </Text>
        </Box>
      </Box>
    </Box>
  );

export default HospitalInfoCard;
