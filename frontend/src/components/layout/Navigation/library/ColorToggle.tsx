import styled from '@emotion/styled'
import { themeProps, Box, Text, UnstyledButton } from 'components/design-system'

const ColorToggleWrapper = styled(Box)<Pick<ColorToggleProps, 'isOptionMenu'>>`
  display: ${props => (props.isOptionMenu ? 'flex' : 'none')};
  justify-content: space-between;
  align-items: center;
  ${themeProps.mediaQueries.sm} {
    display: flex;
  }
`

const ToggleButtonInnerWrapper = styled.div`
  display: flex;
  width: 134px;
  height: 32px;
  font-weight: bold;
  line-height: 1;
`

const ToggleButtonText = styled(Text)`
  display: none;

  ${themeProps.mediaQueries.lg} {
    display: block;
  }
`

const ToggleButtonLight = styled(Box)`
  display: flex;
  flex: 1;
  justify-content: center;
  align-items: center;
  border-radius: 16px 0 0 16px;
`

const ToggleButtonDark = styled(Box)`
  display: flex;
  flex: 1;
  justify-content: center;
  align-items: center;
  border-radius: 0 16px 16px 0;
`

interface ColorToggleProps {
  isDarkMode: boolean
  toggleDarkMode: () => void
  isOptionMenu?: boolean
}

const ColorToggle: React.FC<ColorToggleProps> = ({ isDarkMode, toggleDarkMode, isOptionMenu }) => {
  const lightColor = isDarkMode ? 'accents03' : 'brandblue'
  const darkColor = isDarkMode ? 'brandblue' : 'accents03'

  return (
    <ColorToggleWrapper isOptionMenu={isOptionMenu}>
      <ToggleButtonText variant={200} color="foreground" mr="sm">
        Mode Warna
      </ToggleButtonText>
      <UnstyledButton style={{ outline: 'none' }} onClick={toggleDarkMode}>
        <ToggleButtonInnerWrapper>
          <ToggleButtonLight backgroundColor={lightColor} color={themeProps.colors.foreground}>
            <Text variant={200}>Terang</Text>
          </ToggleButtonLight>
          <ToggleButtonDark backgroundColor={darkColor} color={themeProps.colors.foreground}>
            <Text variant={200}>Gelap</Text>
          </ToggleButtonDark>
        </ToggleButtonInnerWrapper>
      </UnstyledButton>
    </ColorToggleWrapper>
  )
}

export default ColorToggle
