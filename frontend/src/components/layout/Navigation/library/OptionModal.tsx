import * as React from 'react'
import useDarkMode from 'utils/useDarkMode'
import styled from '@emotion/styled'
import { Portal, UnstyledButton, Box } from 'components/design-system'
import NavLinkVertical from 'components/layout/Navigation/NavLinkVertical'
import Logo from 'components/layout/Navigation/Logo'
import { CloseIcon, LockIcon, MobileIcon, ProfileIcon } from 'components/icons'
import { NavGrid, NavInner } from '.'
import ColorToggle from './ColorToggle'

export const OptionModalButton = styled(UnstyledButton)``

interface OptionModalProps {
  isOpen?: boolean
  onClose?: () => void
}

const Container = styled(Box)`
  overflow-x: hidden;
  overflow-y: auto;
`

const VerticalNavGrid = styled(NavGrid)`
  display: flex;
  flex-direction: column;
  flex: 0;
`

const NavInnerHeader = styled(NavInner)`
  font-size: 12px;
  line-height: 16px;
  font-weight: bold;
  margin: 16px 0;
`

const NavLabel = styled(Box)`
  margin: 0;
`

const Footer = styled(Box)`
  display: flex;
  justify-content: flex-end;
  align-items: center;
  position: absolute;
  bottom: -60px;
  width: 100%;
  height: 60px;
  background: rgb(34, 39, 45, 0.8);
`

const CloseButtonContainer = styled(UnstyledButton)`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  width: calc(100% / 3);
  height: 100%;
  font-size: 10px;
  font-weight: 600;
  padding-bottom: 4px;
`

const CloseButtonIconWrapper = styled(Box)`
  > svg {
    margin-top: 1px;
    margin-left: -1px;
  }

  margin-bottom: 4px;
`

const NavInnerFooter = styled(NavInnerHeader)`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  margin: 16px 0;
`

const OptionModal: React.FC<OptionModalProps> = ({ isOpen, onClose }) => {
  const [isDarkMode, toggleDarkMode] = useDarkMode()

  React.useEffect(() => {
    document.body.classList.toggle('noscroll', isOpen)
  }, [isOpen])

  const toggleModal = () => {
    if (onClose) {
      onClose()
    }
  }

  const colorToggleProps = {
    isDarkMode,
    toggleDarkMode,
    isOptionMenu: true
  }

  const renderInnerContent = () => {
    if (isOpen) {
      return (
        <Box
          display="flex"
          flexDirection="column"
          position="fixed"
          top={0}
          left={0}
          width="100%"
          height="calc(100% - 60px)"
          backgroundColor="background"
          color="foreground"
          zIndex={50}
        >
          <NavGrid backgroundColor="background" color="foreground">
            <NavInner display="flex" flexDirection="row">
              <Box display="flex" flexDirection="row" alignItems="center" flex="1 1 auto" height={60}>
                <Logo aria-hidden />
              </Box>
            </NavInner>
          </NavGrid>
          <Container>
            <VerticalNavGrid backgroundColor="background" color="foreground" flex="1 1 auto">
              <NavInnerHeader display="flex" flexDirection="column">
                Seputar COVID19
              </NavInnerHeader>
              <NavLinkVertical
                href="https://kawalcovid19.id/unduh-aplikasi"
                title="Unduh Aplikasi Mobile"
                icon={<MobileIcon fill={isDarkMode ? '#666b73' : '#666B73'} />}
                isDark={isDarkMode}
              />
              <NavInnerHeader display="flex" flexDirection="column">
                KawalCOVID19
              </NavInnerHeader>
              <NavLinkVertical
                href="https://kawalcovid19.id/tentang-kami"
                title="Tentang Kami"
                icon={<ProfileIcon fill={isDarkMode ? '#666b73' : '#666B73'} />}
                isDark={isDarkMode}
              />
              <NavLinkVertical
                href="https://kawalcovid19.id/kebijakan-privasi"
                title="Kebijakan Privasi"
                icon={<LockIcon fill={isDarkMode ? '#666b73' : '#666B73'} />}
                isDark={isDarkMode}
              />
            </VerticalNavGrid>
            <VerticalNavGrid backgroundColor="background" color="foreground" flex="1 1 auto">
              <NavInnerFooter display="flex" flexDirection="column">
                <NavLabel>Ganti mode warna</NavLabel>
                <Box>
                  <ColorToggle {...colorToggleProps} />
                </Box>
              </NavInnerFooter>
            </VerticalNavGrid>
          </Container>
          <Footer>
            <CloseButtonContainer type="button" backgroundColor="background" onClick={toggleModal}>
              <CloseButtonIconWrapper
                display="flex"
                alignItems="center"
                justifyContent="center"
                size={20}
                borderRadius={20}
                backgroundColor="brandred"
              >
                <CloseIcon />
              </CloseButtonIconWrapper>
              Tutup
            </CloseButtonContainer>
          </Footer>
        </Box>
      )
    }

    return null
  }

  return <Portal>{renderInnerContent()}</Portal>
}

OptionModal.defaultProps = {
  isOpen: false,
  onClose: undefined
}

export default OptionModal
