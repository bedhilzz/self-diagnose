import * as React from 'react'
import Link from 'next/link'
import { useRouter } from 'next/router'
import styled from '@emotion/styled'
import VisuallyHidden from '@reach/visually-hidden'

import { themeProps, Box, UnstyledButton, Text } from 'components/design-system'
import { logEventClick } from 'utils/analytics'
import useDarkMode from 'utils/useDarkMode'
import Logo from './Logo'
import SearchModal from './library/SearchModal'
import { NavGrid, MainNavInner, MainNavCenter, MainNavCenterLinks, MainNavLink, MainNavRight, MobileNav, MobileNavLink } from './library'
import { ChevronIcon, SearchIcon, HomeIcon, OptionIcon, HealthMeterIcon } from '../../icons'
import ColorToggle from './library/ColorToggle'
import OptionModal from './library/OptionModal'

interface NavigationProps {
  pageTitle?: string
}

const Root = Box.withComponent('header')

const LogoLinkRoot = Box.withComponent('a')

const LogoLink = styled(LogoLinkRoot)`
  display: block;
  width: 56px;
  height: 48px;
  overflow: hidden;

  ${themeProps.mediaQueries.md} {
    width: 80px;
    height: 64px;
  }

  &:hover,
  &:focus,
  &:active,
  &:visited {
    text-decoration: none;
  }
`

const LogoWrapper = styled(Box)`
  > svg {
    ${themeProps.mediaQueries.md} {
      width: 80px !important;
      height: 64px !important;
    }
  }
`

const PageTitle = styled(Box)`
  display: flex;
  flex-direction: row;
  align-items: center;

  ${themeProps.mediaQueries.sm} {
    display: none;
  }
`

const SearchButton = styled(UnstyledButton)`
  display: inline-flex;
  align-items: center;
  justify-content: center;
  width: 40px;
  height: 40px;
  outline: none;
`

const OptionButton = styled(UnstyledButton)`
  display: flex;
  flex-direction: column;
  flex: 1;
  justify-content: center;
  align-items: center;
  font-size: 10px;
  font-weight: 600;
  padding-bottom: 8px;
`

const OptionButtonIcon = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  width: 24px;
  height: 24px;
  margin-bottom: 4px;
`

const Navigation: React.FC<NavigationProps> = ({ pageTitle }) => {
  const [isDarkMode, toggleDarkMode] = useDarkMode()
  const [isSearchModalOpen, setIsSearchModalOpen] = React.useState(false)
  const [isOptionModalOpen, setIsOptionModalOpen] = React.useState(false)
  const router = useRouter()

  const toggleSearchModal = () => {
    setIsSearchModalOpen(!isSearchModalOpen)
  }

  const toggleOptionModal = () => {
    setIsOptionModalOpen(!isOptionModalOpen)
  }

  const colorToggleProps = {
    isDarkMode,
    toggleDarkMode
  }

  const { pathname } = router
  const mobileButtonColor = (path: string) => {
    if (pathname === path) {
      return '#3389fe'
    }

    return '#f1f2f3'
  }

  return (
    <Root>
      <NavGrid backgroundColor="accents01">
        <MainNavInner>
          <Link href="/" passHref>
            <LogoLink onClick={() => logEventClick('Beranda')}>
              <VisuallyHidden>Kawal COVID-19</VisuallyHidden>
              <LogoWrapper display="flex" alignItems="flex-end">
                <Logo aria-hidden />
              </LogoWrapper>
            </LogoLink>
          </Link>
          <MainNavCenter flex="1 1 auto">
            {pageTitle && (
              <PageTitle>
                <ChevronIcon fill={themeProps.colors.accents07} />
                <Text ml="sm">{pageTitle}</Text>
              </PageTitle>
            )}
            <MainNavCenterLinks>
              <Link href="/" passHref>
                <MainNavLink isActive={pathname === '/'}>Beranda</MainNavLink>
              </Link>
              <Link href="/periksa-mandiri" passHref>
                <MainNavLink isActive={pathname === '/periksa-mandiri'}>Periksa Mandiri</MainNavLink>
              </Link>
              {/* <Link href="/rs-rujukan" passHref>
                <MainNavLink isActive={pathname === '/rs-rujukan'}>RS Rujukan</MainNavLink>
              </Link>
              <Link href="/panduan" passHref>
                <MainNavLink isActive={pathname === '/panduan'}>Panduan</MainNavLink>
              </Link> */}
            </MainNavCenterLinks>
          </MainNavCenter>
          <MainNavRight display="flex" alignItems="center">
            <ColorToggle {...colorToggleProps} />
            <SearchButton
              type="button"
              backgroundColor="accents01"
              onClick={toggleSearchModal}
              display={['none', null, 'flex', null, null]}
              ml="md"
            >
              <VisuallyHidden>Pencarian</VisuallyHidden>
              <SearchIcon fill={isDarkMode ? '#f1f2f3' : '#22272c'} aria-hidden />
            </SearchButton>
          </MainNavRight>
        </MainNavInner>
      </NavGrid>
      <NavGrid backgroundColor="accents01">
        <MobileNav backgroundColor="accents01">
          <MobileNavLink
            href="/"
            isActive={pathname === '/'}
            title="Beranda"
            icon={<HomeIcon width={24} fill={mobileButtonColor('/')} />}
          />
          {/* <MobileNavLink
            href="/rs-rujukan"
            isActive={pathname === '/rs-rujukan'}
            title="RS Rujukan"
            icon={<MedicalIcon width={24} fill={mobileButtonColor('/rs-rujukan')} />}
          /> */}
          <MobileNavLink
            href="/periksa-mandiri"
            isActive={pathname === '/periksa-mandiri'}
            title="Cek Mandiri"
            icon={<HealthMeterIcon width={24} fill={mobileButtonColor('/periksa-mandiri')} />}
          />
          {/* <MobileNavLink
            href="/panduan"
            isActive={pathname === '/panduan'}
            title="Karantina"
            icon={<QuarantineIcon width={24} fill={mobileButtonColor('/panduan')} />}
          /> */}
          <OptionButton type="button" style={{ outline: 'none' }} onClick={toggleOptionModal}>
            <OptionButtonIcon>
              <OptionIcon width={24} fill={isDarkMode ? '#f1f2f3' : '#22272c'} />
            </OptionButtonIcon>
            Lainnya
          </OptionButton>
        </MobileNav>
      </NavGrid>
      <SearchModal isOpen={isSearchModalOpen} onClose={toggleSearchModal} />
      <OptionModal isOpen={isOptionModalOpen} onClose={toggleOptionModal} />
    </Root>
  )
}

export default Navigation
