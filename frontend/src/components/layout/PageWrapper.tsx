/* eslint-disable react/no-array-index-key */

import * as React from 'react'
import Head from 'next/head'

import { initGA, logPageView } from 'utils/analytics'
import usePrefetch from 'utils/usePrefetch'
// import QiscusWidget from 'modules/core/QiscusWidget';

import LayoutRoot from './LayoutRoot'
import { Navigation } from './Navigation'
import Footer from './Footer'

interface PageWrapperProps {
  title?: string
  pageTitle?: string
  author?: string
  description?: string
}

declare global {
  interface Window {
    GA_INITIALIZED?: boolean
  }
}

const defaultTitle = 'Informasi Terkini COVID-19 di Indonesia | KawalCOVID19'
const defaultAuthor = 'KawalCOVID19'
const defaultDescription =
  'Kawal informasi seputar COVID-19 secara tepat dan akurat. Situs ini merupakan sumber informasi inisiatif sukarela netizen Indonesia pro-data; terdiri dari praktisi kesehatan, akademisi & profesional.'
const defaultKeywords = 'coronavirus, corona, covid-19, covid 19, kawal corona, kawal covid-19, kawal covid 19, kawalcovid19, virus corona'

const PageWrapper: React.FC<PageWrapperProps> = ({
  author = defaultAuthor,
  children,
  title = defaultTitle,
  pageTitle,
  description = defaultDescription
}) => {
  const metaAttributes = [
    {
      name: `description`,
      content: description
    },
    {
      name: 'keywords',
      content: defaultKeywords
    },
    {
      property: `og:title`,
      content: title
    },
    {
      property: `og:description`,
      content: description
    },
    {
      property: `og:image`,
      content: `https://kawalcovid19.id/android-chrome-192x192.png`
    },
    {
      property: `og:image:width`,
      content: `192`
    },
    {
      property: `og:image:height`,
      content: `192`
    },
    {
      property: `og:type`,
      content: `website`
    },
    {
      name: `twitter:card`,
      content: `summary`
    },
    {
      name: `twitter:creator`,
      content: author
    },
    {
      name: `twitter:title`,
      content: title
    },
    {
      name: `twitter:description`,
      content: description
    }
  ]

  // Preload Qiscus assets
  // usePrefetch('preload', 'https://rsms.me/inter/inter.css', 'style');
  // usePrefetch('preload', 'https://fonts.googleapis.com/css?family=Open+Sans:400,600', 'style');

  // Preconnect required resources
  usePrefetch('preconnect', 'https://qismo.qiscus.com')
  usePrefetch('preconnect', 'https://qiscus-sdk.s3-ap-southeast-1.amazonaws.com')
  usePrefetch('preconnect', 'https://s3-ap-southeast-1.amazonaws.com')

  React.useEffect(() => {
    if (typeof window !== 'undefined' && !window.GA_INITIALIZED) {
      initGA()
      window.GA_INITIALIZED = true
    }
    logPageView()
  }, [])

  return (
    <LayoutRoot>
      <Head>
        <title>{title}</title>
        {metaAttributes.map(attributes => (
          <meta key={attributes.name || attributes.property} {...attributes} />
        ))}
        {/* <QiscusWidget /> */}
      </Head>
      <Navigation pageTitle={pageTitle} />
      {children}
      <Footer />
    </LayoutRoot>
  )
}

export default PageWrapper
