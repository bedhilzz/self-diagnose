import * as React from 'react'
import { NextPage } from 'next'
import styled from '@emotion/styled'

import { PageWrapper, Content, Column, Container } from 'components/layout'
import { Stack, Heading, themeProps } from 'components/design-system'

const Section = Content.withComponent('section')

const IframeContainer = styled(Container)`
  overflow: hidden;
  padding-top: 133.33%;
  position: relative;

  ${themeProps.mediaQueries.lg} {
    padding-top: 56.25%;
  }

  & iframe {
    border: 0;
    height: 100%;
    left: 0;
    position: absolute;
    top: 0;
    width: 100%;
  }
`

const SurveyPage: NextPage<{}> = () => (
  <PageWrapper pageTitle="Periksa Mandiri">
    <Section>
      <Column>
        <Stack spacing="xxl">
          <Container style={{ marginBottom: 24 }}>
            <Heading>Periksa Mandiri</Heading>
          </Container>
          <IframeContainer>
            <iframe src="https://feedloop.io/q/covid19selfassesment" />
          </IframeContainer>
        </Stack>
      </Column>
    </Section>
  </PageWrapper>
)

export default SurveyPage
