import React from 'react';
import { NextPage } from 'next';

import { Content, PageWrapper, Column } from 'components/layout';
import { UnstyledButton } from 'components/design-system/components/Common';
import { Stack } from 'components/design-system/components/Stack';
import { Text } from 'components/design-system/components/Typography';
import { Box } from 'components/design-system/components/Box';
import { HospitalInfoCard } from 'components/layout/HospitalInfoCard';
import styled from '@emotion/styled';
import { themeGet } from '@styled-system/theme-get';
import { themeProps } from 'components/design-system/Theme';
import { fetch } from 'utils/api';

const Section = Content.withComponent('section');
const SearchInput = styled('input')`
  display: inline-flex;
  width: 100%;
  height: 60px;
  align-items: center;
  padding-left: 60px;
  padding-right: 20px;
  box-sizing: border-box;
  border: 1px solid transparent;
  border-radius: 5px;
  background-color: ${themeGet('colors.accents02', themeProps.colors.accents02)};
  color: ${themeGet('colors.foreground', themeProps.colors.foreground)};
  background-image: url("/icon-search.svg");
  background-repeat: no-repeat;
  background-size: 20px 20px;
  background-position: 20px 20px;
  outline: none;

  &:focus {
    background-color: ${themeGet('colors.accents03', themeProps.colors.accents03)};
  }

  &::-webkit-search-cancel-button{
    margin-right: 0px;
    -webkit-appearance: none;
    height: 30px;
    width: 30px;
    border-radius: 15px;
    background-image: url("/icon-close.svg");
    background-repeat: no-repeat;
    background-size: 20px 20px;
    background-position: 5px 5px;
  }
`;

const Button = styled(UnstyledButton)`
  width: 100%;
  height: 60px;
  display: inline-flex;
  align-items: center;
  justify-content: center;
  border-radius: 5px;

  &.disabled, &[disabled] {
    background-color: ${themeGet('colors.primary03', themeProps.colors.primary03)};
  }
`;

const Spinner = styled('div')`
  display: block;
  margin-left: auto;
  margin-right: auto;
  border: 5px solid transparent;
  border-top: 5px solid ${themeGet('colors.primary02', themeProps.colors.primary02)};
  border-left: 5px solid ${themeGet('colors.primary02', themeProps.colors.primary02)};
  border-right: 5px solid ${themeGet('colors.primary02', themeProps.colors.primary02)};
  border-radius: 50%;
  width: 50px;
  height: 50px;
  animation: spin 1s linear infinite;

  @keyframes spin {
    0% { transform: rotate(0deg); }
    100% { transform: rotate(360deg); }
  }
`;

interface Hospital {
  id: number;
  province: string;
  city: string;
  name: string;
  address: string;
  phone: string;
  description: string;
  lat: number;
  long: number;
}

const HospitalReferences: NextPage<{}> = () => {
  const BASE_URL = 'https://cekdiri.kawalcovid19.id';
  const fetchHospitalData = (url: string = BASE_URL) => {
    setNumberOfItems(10);
    setLoading(true);
    fetch(url)
      .then((result: Hospital[]) => {
        setHospitals(result);
        setLoading(false);
      })
      .catch((error) => {
        console.log(error);
        setLoading(false);
      });
  }

  const fetchHospitals = (keyword: string = '') => {
    const URL = `${BASE_URL}/api/hospitals?query=${keyword}`;
    fetchHospitalData(URL);
  }

  const getNearbyHospitals = () => {
    setSearchKeyword('');
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition((position) => {
        const latitude = position.coords.latitude;
        const longitude = position.coords.longitude;
        const URL = `${BASE_URL}/api/hospitals/nearby?lat=${latitude}&long=${longitude}`;
        fetchHospitalData(URL);
      });
    }
  }

  const [hospitals, setHospitals] = React.useState<Hospital[]>([]);
  const [numberOfItems, setNumberOfItems] = React.useState<number>(10);
  const [textInfo, setTextInfo] = React.useState<string>('');
  const [isLoading, setLoading] = React.useState<boolean>(true);
  const [searchKeyword, setSearchKeyword] = React.useState<string>('');

  React.useEffect(() => { fetchHospitals() }, []);
  React.useEffect(() => {
    if (hospitals.length == 0) {
      if (searchKeyword === '') {
        setTextInfo('Tidak ada data rumah sakit');
      } else {
        setTextInfo(`Data dengan kata kunci "${searchKeyword}" tidak ditemukan. Coba lagi ya.`);
      }
    } else if (hospitals.length > 0) {
      if (searchKeyword === '') {
        setTextInfo('Menampilkan semua rumah sakit');
      } else {
        setTextInfo(`Pencarian untuk kata kunci "${searchKeyword}"`);
      }
    }
  }, [hospitals]);

  return (
    <PageWrapper
      title="Daftar Rumah Sakit | KawalCOVID19"
    >
      <Section noPadding={true}>
        <Column>
          <Stack>
            <Box padding="md">
              <SearchInput
                type="search"
                id="search"
                placeholder="Ketik nama kota, daerah, atau nama rumah sakit"
                borderRadius={5}
                value={searchKeyword}
                onChange={(inputEvent) => setSearchKeyword(inputEvent.target.value)}
              />
              <Text as="p" mt="sm" style={{ textAlign: "right" }}>
                <UnstyledButton disabled={isLoading} onClick={getNearbyHospitals}>
                  <u>Gunakan Lokasi Terdekat</u>
                </UnstyledButton>
              </Text>
              <Button
                type="button"
                mt="sm"
                backgroundColor="primary02"
                disabled={isLoading}
                onClick={() => fetchHospitals(searchKeyword)}
              >
                Cari Rumah Sakit
              </Button>
            </Box>
            <Box padding="md">
              {isLoading && (
                <Box
                  justifyContent="center"
                  textAlign="center"
                >
                  <Spinner />
                  <Text as="p" fontWeight="bold" variant={500} mt="md">
                    Sedang mencari rumah sakit
                  </Text>
                  <Text as="p" color="accents05" mt="sm">
                    Tunggu sebentar
                  </Text>
                </Box>
              )}
              {!isLoading && (<>
                <Text as="p" textAlign={hospitals.length > 0 ? 'left' : 'center'} variant={600}>
                  {textInfo}
                </Text>
                {hospitals.length > 0 && (<>
                  <Text as="p" color="accents05" variant={200} mt="sm">
                    Ada {hospitals.length} total data
                  </Text>
                  {hospitals.length >= 0 && hospitals.slice(0, numberOfItems).map((hospital: Hospital) => {
                    return (<HospitalInfoCard
                      key={`hospital-card--${hospital.id}`}
                      id={hospital.id}
                      name={hospital.name}
                      address={`${hospital.address}, ${hospital.city}, ${hospital.province}`}
                      phoneNumber={hospital.phone}
                    />);
                  })}
                  {((hospitals.length - numberOfItems) > 0) && (
                    <Button
                      type="button"
                      onClick={() => setNumberOfItems(numberOfItems + 10)}
                      backgroundColor="accents02"
                      mt="sm"
                    >
                      Muat lebih banyak
                    </Button>
                  )}
                </>)}
              </>)}
            </Box>
          </Stack>
        </Column>
      </Section>
    </PageWrapper>
  );
}

export default HospitalReferences;
