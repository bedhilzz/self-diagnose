import { IsNotEmpty } from "class-validator";
import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";

@Entity({ name: "hospital" })
export class Hospital {
    @PrimaryGeneratedColumn()
    public id: number;

    @IsNotEmpty()
    @Column({ name: "province" })
    public province: string;

    @IsNotEmpty()
    @Column({ name: "city" })
    public city: string;

    @IsNotEmpty()
    @Column({ name: "name" })
    public name: string;

    @IsNotEmpty()
    @Column({ name: "address" })
    public address: string;

    @IsNotEmpty()
    @Column({ name: "phone" })
    public phone: string;

    @IsNotEmpty()
    @Column({ name: "description", type: "text" })
    public description: string;

    @IsNotEmpty()
    @Column({ name: "lat" })
    public lat: number;

    @IsNotEmpty()
    @Column({ name: "long" })
    public long: number;

    public distance?: number;
}
