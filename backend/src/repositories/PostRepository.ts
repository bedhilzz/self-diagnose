import { EntityRepository, Repository } from "typeorm";
import { Post } from "../entities/Post";
import { cache } from "../utils/Cache"

@EntityRepository(Post)
export class PostRepository extends Repository<Post> {
    public async getPosts(offset: number, limit: number) {

        let key: string = `getPosts_${offset}_${limit}`;

        if(cache.has(key)){
            return cache.get(key);
        }else{
            let query = this.createQueryBuilder("post")
            .select(["post.id", "post.title", "post.previewContent", "post.createdAt", "post.view", "post.like"])
            .leftJoinAndSelect("post.user", "user")
            .orderBy("post.createdAt", "DESC")
            .skip(offset)
            .take(limit)
            .getMany();

            cache.set(key,query);
            return query;
        }
    }

    public async getPostById(postId: string) {

        let key: string = `getPostId_${postId}`;

        if(cache.has(key)){
            return cache.get(key);
        }else{
             let query = this.createQueryBuilder("post")
            .leftJoinAndSelect("post.user", "user")
            .where("post.id = :postId", { postId })
            .getOne();

            cache.set(key,query);
            return query;
        }

    }
}
