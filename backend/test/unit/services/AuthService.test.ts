import { Connection } from "typeorm";
import { createMemoryDatabase } from "../../utils/CreateMemoryDatabase";
import { AuthService } from "../../../src/services/AuthService";
import { UserRepository } from "../../../src/repositories/UserRepository";
import { User } from "../../../src/entities/User";
import { UserSeed } from "../../utils/seeds/UserTestSeed";

describe("AuthService", () => {
    let db: Connection;
    let userRepository: UserRepository;
    let authService: AuthService;

    beforeAll(async () => {
        db = await createMemoryDatabase();
        userRepository = db.getCustomRepository(UserRepository);
        authService = new AuthService(userRepository);
        await userRepository.save(UserSeed);
    });

    afterAll(() => db.close());

    const request = {
        id: "6d2deecf-a0f7-470f-b31f-ede0024efece",
        realName: "Hello jest",
        email: "hellojest@gmail.com",
        password: "password",
        refreshToken:
            "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOiJjOGM5ODhiOS1hZTNhLTRmNjgtOTVkZi0zNWNjM2JjZDRhNWUiLCJpYXQiOjE1ODMxMTU4OTYsImV4cCI6MTU4NDMyNTQ5Nn0.w9Yze38ys8OIGmde7HEH2Gj_xZe0uIy7Di4Od5zzSP4",
    };

    it("If email and password match, user information is returned", async () => {
        const user = await authService.validateUser(request.email, request.password);
        expect(user).toBeInstanceOf(User);
        expect(user.id).toBe(request.id);
        expect(user.realName).toBe(request.realName);
        expect(user.email).toBe(request.email);
    });

    it("If email and password do not match, undefined is returned.", async () => {
        const user = await authService.validateUser(request.email, "notpassword");
        expect(user).toBeUndefined();
    });

    it("If user ID and token match, user information is returned.", async () => {
        const user = await authService.validateUserToken(request.id, request.refreshToken);
        expect(user).toBeInstanceOf(User);
        expect(user.id).toBe(request.id);
        expect(user.realName).toBe(request.realName);
        expect(user.email).toBe(request.email);
    });

    it("If user ID and token do not match, undefined is returned.", async () => {
        const user = await authService.validateUserToken(request.id, "not token");
        expect(user).toBeUndefined();
    });
});
